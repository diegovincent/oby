import { Component } from '@angular/core';

import { BaseComponent } from '@oby/core';

@Component({
  moduleId: module.id,
  selector: 'oby-home',
  templateUrl: './home.component.html'
})
export class HomeComponent extends BaseComponent {

}
