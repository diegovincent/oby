import { NgModule } from '@angular/core';

// xplat
import { UIModule } from '@oby/nativescript';

@NgModule({
  imports: [UIModule],
  exports: [UIModule]
})
export class SharedModule {}
