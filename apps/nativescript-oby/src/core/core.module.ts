import { NgModule } from '@angular/core';

// libs
import { ObyCoreModule } from '@oby/nativescript';

@NgModule({
  imports: [ObyCoreModule]
})
export class CoreModule {}
